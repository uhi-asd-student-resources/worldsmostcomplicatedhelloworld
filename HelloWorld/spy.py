class LoggingInterface:
  def log(self, msg):
    pass

class LoggingClass(LoggingInterface):
  def log(self, msg):
    print(msg)


class Server:
  def __init__(self, logger):
    self.logger = logger

  def run(self):
    value = 2 + 3
    str = "the value is {}".format(value)
    self.logger.log(str)

