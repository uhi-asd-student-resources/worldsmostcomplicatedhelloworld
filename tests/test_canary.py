import unittest

class TestCanary(unittest.TestCase):

    def test_canary(self):
        self.assertEqual('foo'.upper(), 'FOO')
