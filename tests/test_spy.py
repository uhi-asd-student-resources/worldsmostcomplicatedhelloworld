import unittest
from HelloWorld.spy import LoggingClass, Server, LoggingInterface

# Spy using inheritance
class Spy(LoggingClass):
  def __init__(self):
    self.input = []
    self.count = 0

  def log(self, msg):
    self.input += [msg]
    self.count += 1
    super().log(msg)

# Spy using composition
# - obeys the original interface
# - concrete function we cannot inherit from e.g. if final, passed using dependency injection
class SpyUsingComposition(LoggingInterface):
  def __init__(self, ConcreteLoggingClass):
    self.input = []
    self.count = 0
    self.ConcreteLoggingClass = ConcreteLoggingClass

  def log(self, msg):
    self.input += [msg]
    self.count += 1
    self.ConcreteLoggingClass.log(msg)

class TestSpy(unittest.TestCase):

    def test_spy(self):
        spy = Spy()
        server = Server(spy)
        server.run()
        self.assertEqual(spy.count, 1)
