import unittest
from HelloWorld.ticket import Ticket

class NewYearStub:
  def getDate():
    return "2022-12-31"

class NotNewYearStub:
  def getDate():
    return "2022-12-30"

class TestDummy(unittest.TestCase):

    def test_dummy(self):
        newYearStub = NewYearStub
        ticket = Ticket(newYearStub)
        value = ticket.isNewYear()
        self.assertEqual(value, True)
