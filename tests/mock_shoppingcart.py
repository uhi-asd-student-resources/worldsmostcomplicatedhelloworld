from HelloWorld.shoppingcart import ShoppingServiceInterface, ShoppingCart, ShoppingService


class ShoppingServiceMock(ShoppingServiceInterface):
  def __init__(self, ConcreteShoppingService):
    self.ConcreteShoppingService = ConcreteShoppingService

  def calculateAmount(self, lines):
      return self.ConcreteShoppingService.calculateAmount(lines)

  def _getShoppingCart(lines):
    return [100,200,300]

