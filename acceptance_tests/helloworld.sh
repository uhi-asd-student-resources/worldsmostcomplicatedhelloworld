#!/bin/bash

DEST="../.acceptance_tests"
if [ ! -e "$DEST" ]; then
    mkdir "$DEST" 
fi
python ../helloworld.py > "$DEST/$0.out"
diff $0.expected "$DEST/$0.out"
if [ $? -ne 0 ]; then
    echo "$0 ... Failed"
else
    echo "$0 ... Passed"
fi
