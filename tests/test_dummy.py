import unittest
from HelloWorld.dummy import DummyExample

# this is our stub
class CalculationClassMock:
  def method(self, x):
    return 2

# this is our dummy
class EmptyClass:
  pass

class TestDummy(unittest.TestCase):

    def test_dummy(self):
        cc = CalculationClassMock()
        dummy = EmptyClass()
        de = DummyExample(cc, dummy)
        value = de.run(5)
        self.assertEqual(value, 2)
