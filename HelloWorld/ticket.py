import datetime

class DateUtils:
  def getDate():
    return datetime.date.today()

class Ticket:
  def __init__(self, creationDate):
    self.creationDate = creationDate
  
  def isNewYear(self):
    return self.creationDate.getDate() == "2022-12-31"