import unittest
from HelloWorld.HelloWorldFunction import helloworld

class TestHelloWorld(unittest.TestCase):

    def test_helloworld(self):
        self.assertEqual(helloworld(), "hello world")
