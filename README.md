# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Learning about design patterns

### How do I get set up? ###

install python
```
python -m venv venv
source venv/bin/activate
```

### Running tests

```
python -m unittest discover tests "test_*.py"
```

### Resources for Test Doubles

* https://jesusvalerareales.medium.com/testing-with-test-doubles-7c3abb9eb3f2
* https://martinfowler.com/articles/mocksArentStubs.html#TheDifferenceBetweenMocksAndStubs
* https://www.toptal.com/python/an-introduction-to-mocking-in-python
* https://docs.python.org/3/library/unittest.mock.html

Check out the pattern Dependency Injection for separating classes.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact