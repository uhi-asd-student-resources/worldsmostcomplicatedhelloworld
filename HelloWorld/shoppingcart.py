class ShoppingServiceInterface:
  def calculateAmount(self, lines):
    pass

  def _getShoppingCart(lines):
    pass

class ShoppingCart:
  def transform(lines):
    return lines

class ShoppingService(ShoppingServiceInterface):
    def calculateAmount(self, lines):
        amount = 0
        linesTransformed = self.getShoppingCart(lines)
        
        for line in linesTransformed:
            amount += line.price();
        return amount;


    def _getShoppingCart(lines):
     return ShoppingCart.transform(lines)