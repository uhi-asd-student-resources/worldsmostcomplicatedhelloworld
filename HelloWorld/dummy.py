class DummyExample:
  def __init__(self, CalculationClass, DependentButNotUsedInTestFunction):
    self.calc = CalculationClass
    self.dependent = DependentButNotUsedInTestFunction
  
  def run(self, x):
    return self.calc.method(x)