import unittest
from HelloWorld.HelloWorldClass import HelloWorld

class TestHelloWorldClass(unittest.TestCase):

    def test_helloworldclass(self):
      helloWorldClass = HelloWorld()
      textOutput = helloWorldClass.to_string()
      self.assertEqual(textOutput, "hello world")
