class TreeVisitor:
  def visit(self, visitor:"TreeVisitable"):
    pass

class TreeVisitable:
  def accept(self, visitor: TreeVisitor):
    pass

class HelloWorldVisitor(TreeVisitor):
  def visit(self, caller: TreeVisitable):
    return "hello world"


class GoodbyeVisitor(TreeVisitor):
  def visit(self, caller: TreeVisitable):
    return "goodbye"


class HelloWorldVisitable(TreeVisitable):
  def accept(self, visitor):
    return visitor.visit(self)