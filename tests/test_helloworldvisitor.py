import unittest
from HelloWorld.HelloWorldVisitor import HelloWorldVisitor, HelloWorldVisitable, GoodbyeVisitor

class TestHelloWorldClass(unittest.TestCase):

    def test_helloworldclass(self):
      helloWorldVisitor = HelloWorldVisitor()
      helloWorldVisitable = HelloWorldVisitable()
      textOutput = helloWorldVisitable.accept(helloWorldVisitor)
      self.assertEqual(textOutput, "hello world")

    def test_helloworldclass2(self):
      helloWorldVisitor = GoodbyeVisitor()
      helloWorldVisitable = HelloWorldVisitable()
      textOutput = helloWorldVisitable.accept(helloWorldVisitor)
      self.assertEqual(textOutput, "goodbye")